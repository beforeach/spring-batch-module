/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.spring.batch;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossRole;
import com.foreach.across.core.context.AcrossModuleRole;
import com.foreach.across.core.filters.BeanFilterComposite;
import com.foreach.across.core.filters.ClassBeanFilter;
import com.foreach.across.core.transformers.PrimaryBeanTransformer;
import com.foreach.across.modules.spring.batch.installers.SpringBatchSchemaInstaller;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;

import java.util.Collections;

/**
 * @author Arne Vandamme
 */
@AcrossRole(AcrossModuleRole.INFRASTRUCTURE)
public class SpringBatchModule extends AcrossModule
{
	public static final String NAME = "SpringBatchModule";

	public SpringBatchModule() {
		setExposeFilter(
				new BeanFilterComposite(
						getExposeFilter(),
						new ClassBeanFilter(
								JobRepository.class,
								JobLauncher.class,
								JobRegistry.class,
								StepBuilderFactory.class,
								JobBuilderFactory.class,
								JobExplorer.class
						)
				)
		);

		setExposeTransformer(
				new PrimaryBeanTransformer( Collections.singleton( "jobLauncher" ) )
		);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return "Services for enabling Spring data jobs in modules.";
	}

	@Override
	public Object[] getInstallers() {
		return new Object[] { SpringBatchSchemaInstaller.class };
	}
}
