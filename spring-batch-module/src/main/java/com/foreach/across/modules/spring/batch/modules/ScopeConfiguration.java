/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.spring.batch.modules;

import org.springframework.batch.core.scope.JobScope;
import org.springframework.batch.core.scope.StepScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure the job and step scopes in client modules.
 */
@Configuration
public class ScopeConfiguration
{
	@Bean
	public StepScope stepScope() {
		StepScope stepScope = new StepScope();
		stepScope.setAutoProxy( false );
		return stepScope;
	}

	@Bean
	public JobScope jobScope() {
		JobScope jobScope = new JobScope();
		jobScope.setAutoProxy( false );
		return jobScope;
	}
}
