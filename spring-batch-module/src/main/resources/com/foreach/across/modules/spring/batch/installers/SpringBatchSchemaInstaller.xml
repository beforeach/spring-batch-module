<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright 2014 the original author or authors
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
<databaseChangeLog
		xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
         http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd">


	<changeSet id="201408191330" author="arne" runAlways="true" dbms="oracle">
		<sql>
			ALTER session SET nls_length_semantics=CHAR;
		</sql>
	</changeSet>

	<!-- Spring batch tables -->
	<!-- HSQLDB -->
	<changeSet id="201411121156" author="arne" dbms="hsqldb">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<comment>Creating Spring batch tables on HSQLDB</comment>
		<sql>
			CREATE TABLE BATCH_JOB_INSTANCE (
			JOB_INSTANCE_ID BIGINT IDENTITY NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_NAME VARCHAR(100) NOT NULL,
			JOB_KEY VARCHAR(32) NOT NULL,
			constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION (
			JOB_EXECUTION_ID BIGINT IDENTITY NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_INSTANCE_ID BIGINT NOT NULL,
			CREATE_TIME TIMESTAMP NOT NULL,
			START_TIME TIMESTAMP DEFAULT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED TIMESTAMP,
			JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
			constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
			references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_PARAMS (
			JOB_EXECUTION_ID BIGINT NOT NULL ,
			TYPE_CD VARCHAR(6) NOT NULL ,
			KEY_NAME VARCHAR(100) NOT NULL ,
			STRING_VAL VARCHAR(250) ,
			DATE_VAL TIMESTAMP DEFAULT NULL ,
			LONG_VAL BIGINT ,
			DOUBLE_VAL DOUBLE PRECISION ,
			IDENTIFYING CHAR(1) NOT NULL ,
			constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION (
			STEP_EXECUTION_ID BIGINT IDENTITY NOT NULL PRIMARY KEY ,
			VERSION BIGINT NOT NULL,
			STEP_NAME VARCHAR(100) NOT NULL,
			JOB_EXECUTION_ID BIGINT NOT NULL,
			START_TIME TIMESTAMP NOT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			COMMIT_COUNT BIGINT ,
			READ_COUNT BIGINT ,
			FILTER_COUNT BIGINT ,
			WRITE_COUNT BIGINT ,
			READ_SKIP_COUNT BIGINT ,
			WRITE_SKIP_COUNT BIGINT ,
			PROCESS_SKIP_COUNT BIGINT ,
			ROLLBACK_COUNT BIGINT ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED TIMESTAMP,
			constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT LONGVARCHAR ,
			constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
			references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT LONGVARCHAR ,
			constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_SEQ (
			ID BIGINT IDENTITY
			);
			CREATE TABLE BATCH_JOB_EXECUTION_SEQ (
			ID BIGINT IDENTITY
			);
			CREATE TABLE BATCH_JOB_SEQ (
			ID BIGINT IDENTITY
			);
		</sql>
	</changeSet>

	<!-- MySQL -->
	<changeSet id="201411121157" author="arne" dbms="mysql">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<comment>Creating Spring batch tables on Mysql</comment>
		<sql>
			CREATE TABLE BATCH_JOB_INSTANCE (
			JOB_INSTANCE_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_NAME VARCHAR(100) NOT NULL,
			JOB_KEY VARCHAR(32) NOT NULL,
			constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_JOB_EXECUTION (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_INSTANCE_ID BIGINT NOT NULL,
			CREATE_TIME DATETIME NOT NULL,
			START_TIME DATETIME DEFAULT NULL ,
			END_TIME DATETIME DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED DATETIME,
			JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
			constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
			references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_JOB_EXECUTION_PARAMS (
			JOB_EXECUTION_ID BIGINT NOT NULL ,
			TYPE_CD VARCHAR(6) NOT NULL ,
			KEY_NAME VARCHAR(100) NOT NULL ,
			STRING_VAL VARCHAR(250) ,
			DATE_VAL DATETIME DEFAULT NULL ,
			LONG_VAL BIGINT ,
			DOUBLE_VAL DOUBLE PRECISION ,
			IDENTIFYING CHAR(1) NOT NULL ,
			constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_STEP_EXECUTION (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT NOT NULL,
			STEP_NAME VARCHAR(100) NOT NULL,
			JOB_EXECUTION_ID BIGINT NOT NULL,
			START_TIME DATETIME NOT NULL ,
			END_TIME DATETIME DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			COMMIT_COUNT BIGINT ,
			READ_COUNT BIGINT ,
			FILTER_COUNT BIGINT ,
			WRITE_COUNT BIGINT ,
			READ_SKIP_COUNT BIGINT ,
			WRITE_SKIP_COUNT BIGINT ,
			PROCESS_SKIP_COUNT BIGINT ,
			ROLLBACK_COUNT BIGINT ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED DATETIME,
			constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
			references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ENGINE=InnoDB;

			CREATE TABLE BATCH_STEP_EXECUTION_SEQ (
			ID BIGINT NOT NULL,
			UNIQUE_KEY CHAR(1) NOT NULL,
			constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
			) ENGINE=MYISAM;

			INSERT INTO BATCH_STEP_EXECUTION_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as
			tmp where not exists(select * from BATCH_STEP_EXECUTION_SEQ);

			CREATE TABLE BATCH_JOB_EXECUTION_SEQ (
			ID BIGINT NOT NULL,
			UNIQUE_KEY CHAR(1) NOT NULL,
			constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
			) ENGINE=MYISAM;

			INSERT INTO BATCH_JOB_EXECUTION_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as
			tmp where not exists(select * from BATCH_JOB_EXECUTION_SEQ);

			CREATE TABLE BATCH_JOB_SEQ (
			ID BIGINT NOT NULL,
			UNIQUE_KEY CHAR(1) NOT NULL,
			constraint UNIQUE_KEY_UN unique (UNIQUE_KEY)
			) ENGINE=MYISAM;

			INSERT INTO BATCH_JOB_SEQ (ID, UNIQUE_KEY) select * from (select 0 as ID, '0' as UNIQUE_KEY) as tmp where
			not exists(select * from BATCH_JOB_SEQ);
		</sql>
	</changeSet>

	<!-- Oracle -->
	<changeSet id="201411121158" author="arne" dbms="oracle">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<comment>Creating Spring batch tables on Oracle</comment>
		<sql>
			CREATE TABLE BATCH_JOB_INSTANCE (
			JOB_INSTANCE_ID NUMBER(19,0) NOT NULL PRIMARY KEY ,
			VERSION NUMBER(19,0) ,
			JOB_NAME VARCHAR2(100) NOT NULL,
			JOB_KEY VARCHAR2(32) NOT NULL,
			constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION (
			JOB_EXECUTION_ID NUMBER(19,0) NOT NULL PRIMARY KEY ,
			VERSION NUMBER(19,0) ,
			JOB_INSTANCE_ID NUMBER(19,0) NOT NULL,
			CREATE_TIME TIMESTAMP NOT NULL,
			START_TIME TIMESTAMP DEFAULT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR2(10) ,
			EXIT_CODE VARCHAR2(2500) ,
			EXIT_MESSAGE VARCHAR2(2500) ,
			LAST_UPDATED TIMESTAMP,
			JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
			constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
			references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_PARAMS (
			JOB_EXECUTION_ID NUMBER(19,0) NOT NULL ,
			TYPE_CD VARCHAR2(6) NOT NULL ,
			KEY_NAME VARCHAR2(100) NOT NULL ,
			STRING_VAL VARCHAR2(250) ,
			DATE_VAL TIMESTAMP DEFAULT NULL ,
			LONG_VAL NUMBER(19,0) ,
			DOUBLE_VAL NUMBER ,
			IDENTIFYING CHAR(1) NOT NULL ,
			constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION (
			STEP_EXECUTION_ID NUMBER(19,0) NOT NULL PRIMARY KEY ,
			VERSION NUMBER(19,0) NOT NULL,
			STEP_NAME VARCHAR2(100) NOT NULL,
			JOB_EXECUTION_ID NUMBER(19,0) NOT NULL,
			START_TIME TIMESTAMP NOT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR2(10) ,
			COMMIT_COUNT NUMBER(19,0) ,
			READ_COUNT NUMBER(19,0) ,
			FILTER_COUNT NUMBER(19,0) ,
			WRITE_COUNT NUMBER(19,0) ,
			READ_SKIP_COUNT NUMBER(19,0) ,
			WRITE_SKIP_COUNT NUMBER(19,0) ,
			PROCESS_SKIP_COUNT NUMBER(19,0) ,
			ROLLBACK_COUNT NUMBER(19,0) ,
			EXIT_CODE VARCHAR2(2500) ,
			EXIT_MESSAGE VARCHAR2(2500) ,
			LAST_UPDATED TIMESTAMP,
			constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT (
			STEP_EXECUTION_ID NUMBER(19,0) NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR2(2500) NOT NULL,
			SERIALIZED_CONTEXT CLOB ,
			constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
			references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT (
			JOB_EXECUTION_ID NUMBER(19,0) NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR2(2500) NOT NULL,
			SERIALIZED_CONTEXT CLOB ,
			constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE SEQUENCE BATCH_STEP_EXECUTION_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
			CREATE SEQUENCE BATCH_JOB_EXECUTION_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
			CREATE SEQUENCE BATCH_JOB_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;

		</sql>
	</changeSet>

	<!-- SQL Server -->
	<changeSet id="201411121159" author="arne" dbms="mssql">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<comment>Creating Spring batch tables on SQL Server</comment>
		<sql>
			CREATE TABLE BATCH_JOB_INSTANCE (
			JOB_INSTANCE_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_NAME VARCHAR(100) NOT NULL,
			JOB_KEY VARCHAR(32) NOT NULL,
			constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_INSTANCE_ID BIGINT NOT NULL,
			CREATE_TIME DATETIME NOT NULL,
			START_TIME DATETIME DEFAULT NULL ,
			END_TIME DATETIME DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED DATETIME,
			JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
			constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
			references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_PARAMS (
			JOB_EXECUTION_ID BIGINT NOT NULL ,
			TYPE_CD VARCHAR(6) NOT NULL ,
			KEY_NAME VARCHAR(100) NOT NULL ,
			STRING_VAL VARCHAR(250) ,
			DATE_VAL DATETIME DEFAULT NULL ,
			LONG_VAL BIGINT ,
			DOUBLE_VAL DOUBLE PRECISION ,
			IDENTIFYING CHAR(1) NOT NULL ,
			constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY ,
			VERSION BIGINT NOT NULL,
			STEP_NAME VARCHAR(100) NOT NULL,
			JOB_EXECUTION_ID BIGINT NOT NULL,
			START_TIME DATETIME NOT NULL ,
			END_TIME DATETIME DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			COMMIT_COUNT BIGINT ,
			READ_COUNT BIGINT ,
			FILTER_COUNT BIGINT ,
			WRITE_COUNT BIGINT ,
			READ_SKIP_COUNT BIGINT ,
			WRITE_SKIP_COUNT BIGINT ,
			PROCESS_SKIP_COUNT BIGINT ,
			ROLLBACK_COUNT BIGINT ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED DATETIME,
			constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
			references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_SEQ (ID BIGINT IDENTITY);
			CREATE TABLE BATCH_JOB_EXECUTION_SEQ (ID BIGINT IDENTITY);
			CREATE TABLE BATCH_JOB_SEQ (ID BIGINT IDENTITY);
		</sql>
	</changeSet>

	<!-- Postgres -->
	<changeSet id="201901301425" author="marc" dbms="postgresql">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<comment>Creating Spring batch tables on Postgres</comment>
		<sql>
			CREATE TABLE BATCH_JOB_INSTANCE  (
			JOB_INSTANCE_ID BIGINT  NOT NULL PRIMARY KEY ,
			VERSION BIGINT ,
			JOB_NAME VARCHAR(100) NOT NULL,
			JOB_KEY VARCHAR(32) NOT NULL,
			constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION  (
			JOB_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,
			VERSION BIGINT  ,
			JOB_INSTANCE_ID BIGINT NOT NULL,
			CREATE_TIME TIMESTAMP NOT NULL,
			START_TIME TIMESTAMP DEFAULT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED TIMESTAMP,
			JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
			constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
			references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_PARAMS  (
			JOB_EXECUTION_ID BIGINT NOT NULL ,
			TYPE_CD VARCHAR(6) NOT NULL ,
			KEY_NAME VARCHAR(100) NOT NULL ,
			STRING_VAL VARCHAR(250) ,
			DATE_VAL TIMESTAMP DEFAULT NULL ,
			LONG_VAL BIGINT ,
			DOUBLE_VAL DOUBLE PRECISION ,
			IDENTIFYING CHAR(1) NOT NULL ,
			constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION  (
			STEP_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,
			VERSION BIGINT NOT NULL,
			STEP_NAME VARCHAR(100) NOT NULL,
			JOB_EXECUTION_ID BIGINT NOT NULL,
			START_TIME TIMESTAMP NOT NULL ,
			END_TIME TIMESTAMP DEFAULT NULL ,
			STATUS VARCHAR(10) ,
			COMMIT_COUNT BIGINT ,
			READ_COUNT BIGINT ,
			FILTER_COUNT BIGINT ,
			WRITE_COUNT BIGINT ,
			READ_SKIP_COUNT BIGINT ,
			WRITE_SKIP_COUNT BIGINT ,
			PROCESS_SKIP_COUNT BIGINT ,
			ROLLBACK_COUNT BIGINT ,
			EXIT_CODE VARCHAR(2500) ,
			EXIT_MESSAGE VARCHAR(2500) ,
			LAST_UPDATED TIMESTAMP,
			constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT  (
			STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
			references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
			) ;

			CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT  (
			JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
			SHORT_CONTEXT VARCHAR(2500) NOT NULL,
			SERIALIZED_CONTEXT TEXT ,
			constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
			references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
			) ;

			CREATE SEQUENCE BATCH_STEP_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;
			CREATE SEQUENCE BATCH_JOB_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;
			CREATE SEQUENCE BATCH_JOB_SEQ MAXVALUE 9223372036854775807 NO CYCLE;
		</sql>
	</changeSet>

	<!-- Create indices -->


	<!-- spring batch info http://docs.spring.io/spring-batch/reference/html/metaDataSchema.html -->
	<!-- BATCH_JOB_INSTANCE -->
	<changeSet id="201701181352" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_INSTANCE"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_JOB_INSTANCE">
			<column name="JOB_INSTANCE_ID" type="BIGINT">
				<constraints primaryKey="true" nullable="false"/>
			</column>
			<column name="VERSION" type="BIGINT"/>
			<column name="JOB_NAME" type="VARCHAR(100)">
				<constraints nullable="false"/>
			</column>
			<column name="JOB_KEY" type="VARCHAR(2500)"/>
		</createTable>
	</changeSet>

	<!-- BATCH_JOB_EXECUTION -->
	<changeSet id="201701181404" author="benny" dbms="h2">
		<preConditions>
			<not>
				<tableExists tableName="BATCH_JOB_EXECUTION"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_JOB_EXECUTION">
			<column name="JOB_EXECUTION_ID" type="BIGINT">
				<constraints primaryKey="true" nullable="false"/>
			</column>
			<column name="VERSION" type="BIGINT"/>
			<column name="JOB_INSTANCE_ID" type="BIGINT">
				<constraints nullable="false"/>
			</column>
			<column name="CREATE_TIME" type="TIMESTAMP">
				<constraints nullable="false"/>
			</column>
			<column name="START_TIME" type="TIMESTAMP"/>
			<column name="END_TIME" type="TIMESTAMP"/>
			<column name="STATUS" type="VARCHAR(10)"/>
			<column name="EXIT_CODE" type="VARCHAR(20)"/>
			<column name="EXIT_MESSAGE" type="VARCHAR(2500)"/>
			<column name="LAST_UPDATED" type="TIMESTAMP"/>
			<column name="JOB_CONFIGURATION_LOCATION" type="VARCHAR(2500)"/>
		</createTable>
	</changeSet>
	<changeSet id="201701181404b" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<foreignKeyConstraintExists foreignKeyName="JOB_INSTANCE_EXECUTION_FK"/>
			</not>
		</preConditions>
		<addForeignKeyConstraint baseTableName="BATCH_JOB_EXECUTION"
		                         baseColumnNames="JOB_INSTANCE_ID"
		                         constraintName="JOB_INSTANCE_EXECUTION_FK"
		                         referencedTableName="BATCH_JOB_INSTANCE"
		                         referencedColumnNames="JOB_INSTANCE_ID"/>
	</changeSet>

	<!-- BATCH_JOB_EXECUTION_PARAMS -->
	<changeSet id="201701181356" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_EXECUTION_PARAMS"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_JOB_EXECUTION_PARAMS">
			<column name="JOB_EXECUTION_ID" type="BIGINT">
				<constraints nullable="false"/>
			</column>
			<column name="TYPE_CD" type="VARCHAR(6)">
				<constraints nullable="false"/>
			</column>
			<column name="KEY_NAME" type="VARCHAR(100)">
				<constraints nullable="false"/>
			</column>
			<column name="STRING_VAL" type="VARCHAR(250)"/>
			<column name="DATE_VAL" type="DATETIME">
				<constraints nullable="false"/>
			</column>
			<column name="LONG_VAL" type="BIGINT"/>
			<column name="DOUBLE_VAL" type="DOUBLE"/>
			<column name="IDENTIFYING" type="CHAR(1)">
				<constraints nullable="false"/>
			</column>
		</createTable>
	</changeSet>
	<changeSet id="201701181356b" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<foreignKeyConstraintExists foreignKeyName="JOB_EXEC_PARAMS_FK"/>
			</not>
		</preConditions>
		<addForeignKeyConstraint baseTableName="BATCH_JOB_EXECUTION_PARAMS"
		                         baseColumnNames="JOB_EXECUTION_ID"
		                         constraintName="JOB_EXEC_PARAMS_FK"
		                         referencedTableName="BATCH_JOB_EXECUTION"
		                         referencedColumnNames="JOB_EXECUTION_ID"/>
	</changeSet>

	<!-- BATCH_STEP_EXECUTION -->
	<changeSet id="201701181410" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_STEP_EXECUTION"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_STEP_EXECUTION">
			<column name="STEP_EXECUTION_ID" type="BIGINT">
				<constraints primaryKey="true" nullable="false"/>
			</column>
			<column name="VERSION" type="BIGINT">
				<constraints nullable="false"/>
			</column>
			<column name="STEP_NAME" type="VARCHAR(100)">
				<constraints nullable="false"/>
			</column>
			<column name="JOB_EXECUTION_ID" type="BIGINT">
				<constraints nullable="false"/>
			</column>
			<column name="START_TIME" type="TIMESTAMP">
				<constraints nullable="false"/>
			</column>
			<column name="END_TIME" type="TIMESTAMP"/>
			<column name="STATUS" type="VARCHAR(10)"/>
			<column name="COMMIT_COUNT" type="BIGINT"/>
			<column name="READ_COUNT" type="BIGINT"/>
			<column name="FILTER_COUNT" type="BIGINT"/>
			<column name="WRITE_COUNT" type="BIGINT"/>
			<column name="READ_SKIP_COUNT" type="BIGINT"/>
			<column name="WRITE_SKIP_COUNT" type="BIGINT"/>
			<column name="PROCESS_SKIP_COUNT" type="BIGINT"/>
			<column name="ROLLBACK_COUNT" type="BIGINT"/>
			<column name="EXIT_CODE" type="VARCHAR(20)"/>
			<column name="EXIT_MESSAGE" type="VARCHAR(2500)"/>
			<column name="LAST_UPDATED" type="TIMESTAMP"/>
		</createTable>
	</changeSet>
	<changeSet id="201701181410b" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<foreignKeyConstraintExists foreignKeyName="JOB_EXECUTION_STEP_FK"/>
			</not>
		</preConditions>
		<addForeignKeyConstraint baseTableName="BATCH_STEP_EXECUTION"
		                         baseColumnNames="JOB_EXECUTION_ID"
		                         constraintName="JOB_EXECUTION_STEP_FK"
		                         referencedTableName="BATCH_JOB_EXECUTION"
		                         referencedColumnNames="JOB_EXECUTION_ID"/>
	</changeSet>

	<!-- BATCH_JOB_EXECUTION_CONTEXT -->
	<changeSet id="201701181421" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="BATCH_JOB_EXECUTION_CONTEXT"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_JOB_EXECUTION_CONTEXT">
			<column name="JOB_EXECUTION_ID" type="BIGINT">
				<constraints primaryKey="true" nullable="false"/>
			</column>
			<column name="SHORT_CONTEXT" type="VARCHAR(2500)">
				<constraints nullable="false"/>
			</column>
			<column name="SERIALIZED_CONTEXT" type="CLOB"/>
		</createTable>
	</changeSet>
	<changeSet id="201701181421b" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<foreignKeyConstraintExists foreignKeyName="JOB_EXEC_CTX_FK"/>
			</not>
		</preConditions>
		<addForeignKeyConstraint baseTableName="BATCH_STEP_EXECUTION"
		                         baseColumnNames="JOB_EXECUTION_ID"
		                         constraintName="JOB_EXEC_CTX_FK"
		                         referencedTableName="BATCH_JOB_EXECUTION"
		                         referencedColumnNames="JOB_EXECUTION_ID"/>
	</changeSet>

	<!-- BATCH_STEP_EXECUTION_CONTEXT -->
	<changeSet id="201701181416" author="benny" dbms="h2">
		<preConditions>
			<not>
				<tableExists tableName="BATCH_STEP_EXECUTION_CONTEXT"/>
			</not>
		</preConditions>
		<createTable tableName="BATCH_STEP_EXECUTION_CONTEXT">
			<column name="STEP_EXECUTION_ID" type="BIGINT">
				<constraints primaryKey="true" nullable="false"/>
			</column>
			<column name="SHORT_CONTEXT" type="VARCHAR(2500)">
				<constraints nullable="false"/>
			</column>
			<column name="SERIALIZED_CONTEXT" type="CLOB"/>
		</createTable>
	</changeSet>
	<changeSet id="201701181416b" author="benny" dbms="h2">
		<preConditions onFail="MARK_RAN">
			<not>
				<foreignKeyConstraintExists foreignKeyName="STEP_EXEC_CTX_FK"/>
			</not>
		</preConditions>
		<addForeignKeyConstraint baseTableName="BATCH_STEP_EXECUTION_CONTEXT"
		                         baseColumnNames="STEP_EXECUTION_ID"
		                         constraintName="STEP_EXEC_CTX_FK"
		                         referencedTableName="BATCH_STEP_EXECUTION"
		                         referencedColumnNames="STEP_EXECUTION_ID"/>
	</changeSet>

	<!-- BATCH_STEP_EXECUTION_SEQ -->
	<changeSet id="201701181441" author="benny" dbms="h2">
		<preConditions>
			<not>
				<sequenceExists sequenceName="BATCH_STEP_EXECUTION_SEQ"/>
			</not>
		</preConditions>
		<createSequence sequenceName="BATCH_STEP_EXECUTION_SEQ" startValue="0" incrementBy="1"/>
	</changeSet>
	<!-- BATCH_JOB_EXECUTION_SEQ -->
	<changeSet id="201701181442" author="benny" dbms="h2">
		<preConditions>
			<not>
				<sequenceExists sequenceName="BATCH_JOB_EXECUTION_SEQ"/>
			</not>
		</preConditions>
		<createSequence sequenceName="BATCH_JOB_EXECUTION_SEQ" startValue="0" incrementBy="1"/>
	</changeSet>
	<!-- BATCH_JOB_SEQ -->
	<changeSet id="201701181443" author="benny" dbms="h2">
		<preConditions>
			<not>
				<sequenceExists sequenceName="BATCH_JOB_SEQ"/>
			</not>
		</preConditions>
		<createSequence sequenceName="BATCH_JOB_SEQ" startValue="0" incrementBy="1"/>
	</changeSet>

</databaseChangeLog>
