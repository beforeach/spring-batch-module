/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.spring.batch;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.context.AcrossContextUtils;
import com.foreach.across.core.context.configurer.AnnotatedClassConfigurer;
import com.foreach.across.core.context.configurer.ConfigurerScope;
import com.foreach.across.modules.spring.batch.config.SpringBatchConfiguration;
import com.foreach.across.test.AcrossTestContext;
import com.foreach.across.test.support.AcrossTestBuilders;
import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

import static org.junit.Assert.*;

/**
 * @author Arne Vandamme
 */
public class TestTransactionManagerDetection
{
	private final AcrossContextConfigurer defaultConfigurer = context -> context.addModule( new SpringBatchModule() );

	@Test
	public void defaultCreatedIfNoneConfigured() {
		try (AcrossTestContext ctx = AcrossTestBuilders.standard().configurer( defaultConfigurer ).build()) {
			SpringBatchConfiguration config = ctx.getBeanOfTypeFromModule( SpringBatchModule.NAME,
			                                                               SpringBatchConfiguration.class );

			PlatformTransactionManager proxy = config.getTransactionManager();
			assertNotNull( proxy );
			assertNotNull( proxy.toString() );

			PlatformTransactionManager target = (PlatformTransactionManager) AcrossContextUtils.getProxyTarget( proxy );

			assertNotNull( target );
			assertEquals( 1, ctx.getBeansOfType( PlatformTransactionManager.class, true ).size() );
		}
	}

	@Test
	public void existingUsedIfNoneSpecific() {
		AcrossContextConfigurer single = context -> context.addApplicationContextConfigurer(
				new AnnotatedClassConfigurer( SingleTransactionManagerConfig.class ),
				ConfigurerScope.CONTEXT_ONLY
		);

		try (AcrossTestContext ctx = AcrossTestBuilders.standard().configurer( defaultConfigurer, single ).build()) {
			SpringBatchConfiguration config = ctx.getBeanOfTypeFromModule( SpringBatchModule.NAME,
			                                                               SpringBatchConfiguration.class );

			PlatformTransactionManager proxy = config.getTransactionManager();
			assertNotNull( proxy );
			assertNotNull( proxy.toString() );

			PlatformTransactionManager target = (PlatformTransactionManager) AcrossContextUtils.getProxyTarget( proxy );

			assertNotNull( target );
			assertSame( ctx.getBean( "singleTransactionManager" ), target );
			assertEquals( 1, ctx.getBeansOfType( PlatformTransactionManager.class ).size() );
		}
	}

	@Test
	public void multipleCandidatesButOneSpecified() {
		AcrossContextConfigurer multiple = context -> context.addApplicationContextConfigurer(
				new AnnotatedClassConfigurer( SingleTransactionManagerConfig.class,
				                              SpecificTransactionManagerConfig.class ),
				ConfigurerScope.CONTEXT_ONLY
		);

		try (AcrossTestContext ctx = AcrossTestBuilders.standard().configurer( defaultConfigurer, multiple ).build()) {
			SpringBatchConfiguration config = ctx.getBeanOfTypeFromModule( SpringBatchModule.NAME,
			                                                               SpringBatchConfiguration.class );

			PlatformTransactionManager proxy = config.getTransactionManager();
			assertNotNull( proxy );
			assertNotNull( proxy.toString() );

			PlatformTransactionManager target = (PlatformTransactionManager) AcrossContextUtils.getProxyTarget( proxy );

			assertNotNull( target );
			assertSame( ctx.getBean( "otherTransactionManager" ), target );
			assertEquals( 2, ctx.getBeansOfType( PlatformTransactionManager.class ).size() );
		}
	}

	@Configuration
	public static class SingleTransactionManagerConfig
	{
		@Bean
		public DataSourceTransactionManager singleTransactionManager( DataSource dataSource ) {
			return new DataSourceTransactionManager( dataSource );
		}
	}

	@Configuration
	public static class SpecificTransactionManagerConfig
	{
		@Bean(name = { "otherTransactionManager", SpringBatchConfiguration.TRANSACTION_MANAGER_BEAN })
		public DataSourceTransactionManager otherTransactionManager( DataSource dataSource ) {
			return new DataSourceTransactionManager( dataSource );
		}
	}
}
